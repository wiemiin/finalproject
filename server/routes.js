const WELCOME = "/#!/welcome/clients";
const LOGIN = "/#!/login";

var User = require('./models/user');
var Client = require('./models/client');
var Booking = require('./models/booking');
var bCrypt = require('bcrypt-nodejs');


module.exports = function (auth, app, passport) {

  app.post('/login', passport.authenticate('local'), function (req, res) {
    console.log("passport user" + req.user);
    res.status(200).send({
      user: req.user
    });

  });

  app.get('/user/auth', function (req, res, next) {
    console.log(req.user);
    if (req.user) {
      res.status(200).json({
        user: req.user
      });
    } else {
      res.sendStatus(401);
    }
  });

  // app.get('/login', function(req, res, next) {
  //   /* look at the 2nd parameter to the below call */
  //   passport.authenticate('local', function(err, user, info) {
  //     if (err) { return next(err); }
  //     if (!user) { return res.redirect('/login'); }
  //     req.logIn(user, function(err) {
  //       if (err) { return next(err); }
  //       return res.redirect('/users/' + user.username);
  //     });
  //   })(req, res, next);
  // });

  app.get("/oauth/google", passport.authenticate("google", {
    scope: ["email", "profile"]
  }));

  app.get("/oauth/google/callback", passport.authenticate("google", {
    successRedirect: WELCOME,
    failureRedirect: LOGIN
  }));

  app.get('/logout', function (req, res) {
    req.logOut();
    res.redirect('/');
  });

  /**
   * Register USER
   */
  app.post("/api/user", function (req, res) {
    const user = req.body;
    console.log(user);

    User.findOne({ 'local.email': user.email },
      function (err, result) {
        if (err) {
          console.log(err);
          handleError(err, res);
          return;
        }
        if (result) {
          res.status(500).send("Email already exists in database");
        } else {
          var newUser = new User();
          newUser.local.password = generateHash(user.password);
          newUser.local.email = user.email;
          newUser.local.name = user.name;
          newUser.local.mobile = user.mobile;
          newUser.save(function (err, result) {
            res.status(201).send("User added to database");
          });
        }

      });
  });


  /**
   * Register CLIENT
   */
  app.post("/api/client", function (req, res) {
    const client = req.body;
    console.log(client);

    Client.findOne({
      $and: [
             { firstname: client.firstname },
             { lastname: client.lastname }
           ]
    },
      function (err, result) {
        if (err) {
          console.log(err);
          handleError(err, res);
          return;
        }
        if (result) {
          res.status(500).send({msg:"Client already exists in database"});
        } else {
          var newClient = new Client();
          newClient.firstname = client.firstname;
          newClient.lastname = client.lastname;
          newClient.mobile = client.mobile;
          newClient.save(function (err, result) {
            res.status(201).send("Client added to database");
          });
        }

      });
    
  });

    /**
   * Register BOOKING
   */
  app.post("/api/booking", function (req, res) {
    const booking = req.body;
    console.log(booking);

    Booking.create(req.body, function(error, result) {
      if (error) {
        console.log(error);
      }
      res.status(201).send("New booking added to database");


  });
});


  /**
   * SEARCH BY keyword
   */

  app.get("/api/client?", function (req, res) {

    console.log("Search > " + req.query.keyword);
    var keyword = req.query.keyword;
    
        Client.find({ "firstname" : new RegExp('^'+keyword+'$', "i") }, (err, result) => {
          if (err) {
            console.log(err);
          }
          res.status(200).json(result);
          
        });
      });

  /**
   * SEARCH ALL clients
   */

  // app.get("/api/client", function (req, res) {

  //   Client.find({}, (err, result) => {
  //     if (err) {
  //       console.log(err);
  //     }
  //     res.status(200).json(result);
      
  //   });
  // }); 



 /**
   * EDIT client
   */

  app.get("/api/client/:id", function (req, res) {
    const id = req.params.id; 
    console.log(req.params.id);

        Client.findOne({ "_id" : req.params.id }, (err, result) => {
            if (err) {
                console.log(err);
                handleError(err, res);
                return;
            }

            res.status(200).json({ msg: 'Start editing'});
        });
    });

  /**
   * SAVE client
   */

  app.put("/api/client/:id", function (req, res) {

    console.log(req.params.id);
    console.log(req.body.client);

        Client.updateOne({ "_id" : req.params.id }, { $set: req.body.client }, (err, result) => {
            if (err) {
                console.log(err);
                handleError(err, res);
                return;
            }

            res.status(200).json({msg: 'Successfully saved'});
        });
    });

/**
   * DELETE client
   */

   //route
   app.delete("/api/client/:id", function(req, res) {
    console.log(req.params.id);
    //const id = req.params.id.toString(); //to convert type 
    //console.log("Deleting " + id);

    //controller
    Client.deleteOne({ "_id" : req.params.id }, function(err, result) {
      if (err) {
          console.log(err);
          handleError(err, res);
          return;
      }
      res.status(200).json({ msg: 'Successfully deleted'});
  });

  });

  // Generates hash using bCrypt
  // var createHash = function (password) {
  //   return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
  // }

var bcrypt   = require('bcrypt-nodejs');

  var generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

};