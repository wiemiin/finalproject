(function () {
    
    const MongoClient = require('mongodb').MongoClient;

    const url = "mongodb://localhost:27017/peppa";
    const getDB = function (callback) {
        MongoClient.connect(url, {
            poolSize: 10 
        }, function (err, db) {
            if (err) {
                callback(err);
            } else {
                console.log("MongoDB connected")
                callback(db);
            }
        });
    }

    module.exports = { getDB };
})();