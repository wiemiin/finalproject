var mongoose = require('mongoose');

module.exports = mongoose.model('Booking', {
    id: String,
    training: String,
	firstname: String,
	lastname: String,
    date: Date,
    time: Date,
    frequency: String
});