(function () {
    angular
        .module("paApp")
        .service("paAppAPI", [
            '$http',
            "$q",
            paAppAPI
        ]);

    function paAppAPI($http, $q) {
        var self = this;

        self.addUser = function (user) {
            return $http.post("/api/user", user);
        };

        self.addClient = function (client) {
            return $http.post("/api/client", client);
        };

        self.addBooking = function (booking) {
            return $http.post("/api/booking", booking);
        };

        self.searchAllClients = function (term) {
            var defer = $q.defer();

            $http.get("/api/client?keyword=" + term).then(function (result) {
                console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        // self.updateList = function () {
        //     var defer = $q.defer();

        //     $http.get("/api/client").then(function (result) {
        //         console.log(result);
        //         if (result.status == 200) {
        //             defer.resolve(result.data);
        //         } else {
        //             defer.resolve(null);
        //         }

        //     }).catch(function (err) {
        //         console.log(err);
        //         defer.reject(err);
        //     });

        //     return defer.promise;
        // }

        self.getClientById = function (id) {

            var defer = $q.defer();

            $http.get("/api/client/" + id).then(function (result) {
                alert(result.data.msg);
                console.log(result);
                defer.resolve(result);

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        self.updateClient = function (client) {
            var defer = $q.defer();
            const id = client._id; //NOTE THIS!!!!!!!

            console.log('client - before calling http: ', client);
            console.log('id- before calling http: ', id);

            $http.put("/api/client/" + id, {
                    client: client
                })
                .then(function (result) {
                    //alert(result.data.msg);
                    console.log(client);
                    defer.resolve(result);

                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;

        }

        self.deleteClient = function (id) {
            var defer = $q.defer();

            $http.delete("/api/client/" + id).then(function (result) {
                alert(result.data.msg);
                console.log(result);
                defer.resolve(result);

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;

        };
    }

})();