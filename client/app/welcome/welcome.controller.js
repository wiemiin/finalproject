(function () {
  angular
    .module('paApp')
    .controller('WelcomeCtrl', WelcomeCtrl);

  WelcomeCtrl.$inject = ["paAppAPI", "$state", "$scope", 'user'];

  function WelcomeCtrl(paAppAPI, $state, $scope, user) {
    var self = this;

    self.user = user;
    self.client = {};
    self.clients = [];
    self.booking = {};
    self.bookings = [];
    self.current = {};
    self.message = "";

    self.showResult = false;

    self.searchAllClients = function () {

      paAppAPI.searchAllClients(self.term)
        .then(function (result) {
          console.log(result);
          self.clients = result;

        }).catch(function (err) {
          console.log(err);
          if (err.status == 404) {
            self.message = "No clients found";
            self.showMessage = true;
          }
        });

      // paAppAPI.updateList()
      // .then(function (result) {
      //   console.log(result);
      //   self.clients = result;

      // }).catch(function (err) {
      //   console.log(err);
      //   if (err.status == 404) {
      //     self.message = "No clients found";
      //     self.showMessage = true;
      //   }
      // });

    };

    // self.reset = function () {
    //   self.user = Object.assign({}, self.user); 
    //   console.log(self.addClientForm);
    //   self.addClientForm.$setPristine = true;
    //   self.addClientForm.$setUntouched = true;
    // };

    self.addClient = function () {
      self.showSuccessMessage = false;
      self.showFailureMessage = false;

      console.log("Adding client... ");
      console.log(self.client.firstname);
      console.log(self.client.lastname);
      console.log(self.client.mobile);

      //self.reset(); 

      self.showResult = true;
     
      paAppAPI.addClient(self.client)
        .then(function (result) {
          console.log(result);
          self.message = result.data;
          self.showSuccessMessage = true;
          self.clients.push({'firstname': self.client.firstname, 'lastname': self.client.lastname, 'mobile': self.client.mobile});

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });

      // paAppAPI.updateList()
      // .then(function (result) {
      //   console.log(result);
      //   self.clients = result;

      // }).catch(function (err) {
      //   console.log(err);
      //   if (err.status == 404) {
      //     self.message = "No clients found";
      //     self.showMessage = true;
      //   }
      // });

    };

    self.editClient = function (id) {

      console.log("Editing client... ");

      paAppAPI.getClientById(id)
        .then(function (result) {
          console.log(id);
          console.log(result);
          self.message = result;
        }).catch(function (err) {
          console.log(err)
        });

    }

    self.saveClient = function (client) {

      console.log("Saving client... ");
      console.log('client - before calling paAppAPI: ', client);

      paAppAPI.updateClient(client)
        .then(function (result) {
          console.log(result);

          self.message = result.data;
          self.showSuccessMessage = true;

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });

    }

    self.deleteClient = function (id) {
      console.log("Deleting client... ");

      paAppAPI.deleteClient(id)
        .then(function (result) {
          console.log(id);
          console.log(result);
          self.message = result;

        }).catch(function (err) {
          console.log(err);
        });
    };

    self.removeRow = function (clientIndex) {
      self.clients.splice(clientIndex, 1);
    };

    //// BOOKING

    self.addBooking = function () {

      console.log("Adding booking... ");
      console.log(self.booking.training);
      console.log(self.booking.firstname);
      console.log(self.booking.lastname);
      console.log(self.booking.date);
      console.log(self.booking.time);
      console.log(self.booking.frequency);

      self.showResult = true;
      self.booking.date = new Date(self.booking.date);
      self.booking.time = new Date(self.booking.time);
     
      paAppAPI.addBooking(self.booking)
        .then(function (result) {
          console.log(result);
          self.message = result.data;
          self.showSuccessMessage = true;
          self.bookings.push({'training': self.booking.training, 
          'firstname': self.booking.firstname, 
          'lastname': self.booking.lastname, 
          'date': self.booking.date, 
          'time': self.booking.time,
          'frequency': self.booking.frequency,});

        }).catch(function (err) {
          console.log(err);
          self.message = err.data;
          self.showFailureMessage = true;
        });
  }

}

})();